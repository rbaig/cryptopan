/*
* A Java implementation of Crypto-PAn.
*
* Supports both IPv4 and IPv6 anonymization.
*
* Based on:
*   https://www.javatips.net/api/logdb-master/araqne-log-api/src/main/java/org/araqne/log/api/CryptoPAn.java
*   https://github.com/keiichishima/yacryptopan
* 
* Dependencies:
*   https://seancfoley.github.io/IPAddress/
*
* Copyright 2020 Roger Baig
*/

import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import inet.ipaddr.IPAddressNetwork;
import inet.ipaddr.AddressStringException;

import java.math.BigInteger;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
* Anonymize IP addresses keepting prefix consitency.
*/
public class CryptoPAn {

	public static class Mask128 {
		public BigInteger mask;
		public BigInteger pad;
		
		public Mask128(BigInteger mask, BigInteger pad) {
			this.mask = mask;
			this.pad = pad;
		}		
	}
	
	
	private Cipher cipher;
	private Mask128[] masks128;
	
	/** 
	* Class constructor.
	* 
	* @param key A 32 chars string used for AES key and padding when
        *               performing a block cipher operation. The first 16 chars
        *               are used for the AES key, and the latter for padding.
	*/
	public CryptoPAn(String key) {
		if (key.length() != 32) {
			throw new IllegalArgumentException("key must me a 32 byte long string");
		}

		String aesKey = key.substring(0, 16);
		SecretKeySpec keyspec = new SecretKeySpec(aesKey.getBytes(), "AES");
		
		try {

			cipher = javax.crypto.Cipher.getInstance("AES/CBC/NoPadding");
			cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, keyspec,
				new IvParameterSpec(new byte[16]));

			byte[] pad = cipher.doFinal(key.substring(16).getBytes());
			BigInteger f16bp = new BigInteger(pad);
			
			// A 128 bits BigInteger all set to 1
			BigInteger mask128Full =
				(((new BigInteger("0")).setBit(128)).add(new BigInteger("-1")));
			
			this.masks128 = new Mask128[128];
			BigInteger mask = new BigInteger("0");

			// masks128[0]   <- 0000...0000
			// masks128[1]   <- 1000...0000
			// masks128[2]   <- 1100...0000
			// masks128[127] <- 1111...1110
			for (int p = 0; p < 128; ++p) {
				mask = (mask128Full.shiftRight(128 - p)).shiftLeft(128 - p);
				masks128[p] = new Mask128(mask, f16bp.andNot(mask));
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

	}
	
	
	/**
	* Calculate the first bit for Crypto-PAN
	* 
	* @param a value representing the padded address.
	* @return the correspoding bit.
	*/
	private int calc(BigInteger a) throws IllegalBlockSizeException, BadPaddingException {
	
		byte[] pad;
		pad = new byte[16];

		for (int i = 15; i >= 0; i--) {
			pad[i] = (byte) ((a.and(new BigInteger("255")))).intValueExact();
			a = a.shiftRight(8);
		}

		byte[] doFinal = cipher.doFinal(pad);
		return doFinal[0] < 0 ? 1 : 0;
	}

	/**
	* Anonymize an IP address represented as BigInteger value.
	* 
	* @param addri An IP address value. 
	* @param IPv4 1 for IPv4, 0 for IPv6.
	* @return An anoymized IP address value.
	*/
	public byte[] anonymizei(BigInteger addri, boolean IPv4) {

		int pos_max = 0;
		BigInteger ext_addr = new BigInteger("0");

		if (IPv4) {
			pos_max = 32;
			ext_addr = addri.shiftLeft(96);
		} else {
			pos_max = 128;
			ext_addr = addri;
		}

		BigInteger[] addresses = new BigInteger[pos_max];

		try {


			BigInteger result = new BigInteger("0");

			for (int i = 0; i < pos_max; ++i) {
				int calcResult = calc(
					(ext_addr.and(masks128[i].mask)).or(masks128[i].pad));
				result = (result.shiftLeft(1)).or(
					new BigInteger(Integer.toString(calcResult)));
			}

			if (IPv4) result = result.shiftLeft(96);

			byte[] rarr = (result.xor(ext_addr).toByteArray());

			// truncates rarr[17] == -1 of [::,8::) IPv6 addresses
			rarr = Arrays.copyOfRange(rarr, rarr.length - 16, rarr.length);

			// takes the convinient n first bytes (4 for IPv4, 16 for IPv6)
			rarr = Arrays.copyOf(rarr, pos_max/8);

			return rarr;

		} catch (IllegalBlockSizeException e) {
			throw new IllegalStateException(e);
		} catch (BadPaddingException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	* Anonymize an IP address represented as a text string.
	* 
	* @param  an IP address string.
	* @return An anoymized IP address string.
	*/
	public String anonymize(String ip) {

		BigInteger addri = new BigInteger("0");
		boolean IPv4 = true;
		try {
			IPAddressString str = new IPAddressString(ip);
			IPAddress addr = str.toAddress();

			addri = addr.getValue();
			IPv4 = addr.isIPv4();
		} catch(AddressStringException e) {
			System.out.println(e.getMessage());
		}

		byte[] result = anonymizei(addri, IPv4);

		IPAddressNetwork.IPAddressGenerator generator =
			new IPAddressNetwork.IPAddressGenerator();
		IPAddress ipanon = generator.from(result);
		
		return ipanon.toCanonicalString();
	}

	/**
	* Some examples.
	*/
	public static void main(String[] args) throws InterruptedException {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 32; ++i) {
			sb.append((char) i);
		}
		CryptoPAn c = new CryptoPAn(sb.toString());

		String[] IPs = {
			"0.0.0.0",
			"192.0.2.1", "192.0.2.2", "192.0.3.1", "192.1.2.1",
			"32.1.13.184",
			"::",
			"::1",
			"2001:db8::1",
			"8000::",
			"7fff:ffff:ffff:ffff:ffff:ffff:ffff:ffff",

		};
		String[] results = {
			"254.152.65.220",
			"2.90.93.17", "2.90.93.19", "2.90.92.209", "2.91.249.222",
			"221.146.44.68",
			"fe98:41dc:20b0:dd:8002:6000:85ff:800e",
			"fe98:41dc:20b0:dd:8002:6000:85ff:800f",
			"dd92:2c44:3fc0:ff1e:7ff9:c7f0:8180:7e00",
			"7dea:42ff:e0f0:fefc:7001:fbff:5fc:1fe",
			"a800:f86:f806:738f:1c2f:ff1c:3ff8:1ce0",
		};
		for (int i = 0; i < IPs.length; i++) {
			String IPanon = c.anonymize(IPs[i]);		
			System.out.println(
				results[i].equals(IPanon)
				+ " " + IPs[i] + "\t" + IPanon
//				+ "\n(" + results[i] + ")"
			);
		}

		long started = System.currentTimeMillis();
		for (int i = 0; i < 50000; ++i) {
			c.anonymize("192.0.2.1");
		}
		System.out.println("IPv4 elapsed: " + (System.currentTimeMillis() - started));

		started = System.currentTimeMillis();
		for (int i = 0; i < 50000; ++i) {
			c.anonymize("::");
		}
		System.out.println("IPv6 elapsed: " + (System.currentTimeMillis() - started));

	}
}