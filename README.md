# CryptoPAn

An IPv4/IPv6 Java implementation of Crypto-PAn.

Based on:
* https://www.javatips.net/api/logdb-master/araqne-log-api/src/main/java/org/araqne/log/api/CryptoPAn.java (IPv4 only; more efficient)
* https://github.com/keiichishima/yacryptopan (IPv4/IPv6; similar performance)

Dependencies:
* https://seancfoley.github.io/IPAddress/
